#include <iostream>
#include <iomanip>
#define H 30 //Höhe
#define W 30 //Breite
using namespace std;


void showField(bool field[][W]) //Printfunktion
{
     cout << setw(3) << " ";
     for (int p = 0; 5*p < W; p++) cout << setw(5) << 5*p+1;
     cout << endl;
     for (int m = 0; m < H; m++)
     {
         cout << setw(3) << m+1;
         for (int n = 0; n < W; n++)
         {
             if (field[m][n]) cout << "\xDB";
             else cout << /*"\xB1"*/"-";
         }
         cout << endl;
     }
}

void emptyField(bool field[][W]) //Räumt auf
{
    for (int m = 0; m < H; m++)
    {
        for (int n = 0; n < W; n++)
            field[m][n] = 0;
    }
}

void regelnAnwenden(bool field1[][W], bool field2[][W])
{
     unsigned nachbarn;
     for (int m = 0; m < H; m++)
     {
         for (int n = 0; n < W; n++)
         {
             nachbarn = 0;
             //Zählen der Nachbarn
             if (field1[m-1][n-1] == 1) nachbarn++;
             if (field1[m-1][n] == 1) nachbarn++;
             if (field1[m-1][n+1] == 1) nachbarn++;
             if (field1[m][n-1] == 1) nachbarn++;
             if (field1[m][n+1] == 1) nachbarn++;
             if (field1[m+1][n-1] == 1) nachbarn++;
             if (field1[m+1][n] == 1) nachbarn++;
             if (field1[m+1][n+1] == 1) nachbarn++;
             
             //Anwenden unserer Regeln
             if (field1[m][n] == 1 && nachbarn < 2)
                field2[m][n] = 0;
             else if (field1[m][n] == 1 && nachbarn > 3)
                field2[m][n] = 0;
             else if (field1[m][n] == 1 && (nachbarn == 2 || nachbarn == 3))
                field2[m][n] = 1;
             else if (field1[m][n] == 0 && nachbarn == 3)
                field2[m][n] = 1;
         }
     }
}

void swap(bool field1[][W], bool field2[][W]) //Neue Matrix auf die Alte anwenden
{
     for (int m = 0; m < H; m++)
     {
         for (int n = 0; n < W; n++)
             field1[m][n] = field2[m][n];
     }
}
     

int main()
{
    bool now[H][W], next[H][W]; //Initialisieren der Matritzen
    int x, y, cont; //Userinput
    
    
   
    
    cout << "Phase 1:\n - Beliebiges Zeichen eingeben.\n - Zellen durch mehrmalige Koordinateneingabe markieren.\n - \"-1\" eingeben um zu beginnen" << endl;
    cout<< "\n" << endl;
    cout << "Phase 2:\n - beliebige Zahl eingeben um nächste Generation anzuzeigen.\n - \"-1\" eingeben um zu beenden." << endl;
    cin.get();//warten auf User
    
    emptyField(now);
    showField(now);
    
    do //Userinput wird gesammelt
    {
        cin >> x;
        if (x == -1) break; //USer ist fertig
        cin >> y;
        now[y-1][x-1] = 1; //Feld aktivieren
        showField(now); //momentane Belegung des Feldes
    }while(x != -1);
    
    do //Hier läuft das Spiel ab
    {
        emptyField(next);
        regelnAnwenden(now, next);
        swap(now, next);
        showField(now);
        cin>>cont;
    }while(cont != -1);
    
    return 0;
} 